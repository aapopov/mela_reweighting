# Consistent LCG environment (http://lcginfo.cern.ch)
release=`cat /etc/redhat-release`
if test "${release#*release 6.}" != "$release"; then
  # Scientific Linux 6
  . /cvmfs/sft.cern.ch/lcg/views/setupViews.sh LCG_97python3 x86_64-slc6-gcc8-opt
  export SCRAM_ARCH=slc6_amd64_gcc830
  export MAIN_LIB_DIR='/cvmfs/sft.cern.ch/lcg/views/LCG_97python3/x86_64-slc6-gcc8-opt/lib/'
else
  # CentOS 7
  . /cvmfs/sft.cern.ch/lcg/views/setupViews.sh LCG_97python3 x86_64-centos7-gcc9-opt
  export SCRAM_ARCH=slc7_amd64_gcc920
  export MAIN_LIB_DIR='/cvmfs/sft.cern.ch/lcg/views/LCG_97python3/x86_64-centos7-gcc9-opt/lib'
fi

export MELA_DIR="${PWD}/../JHUGenMELA/MELA"
export MELA_ANAL_DIR="${PWD}/../MelaAnalytics"

export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:${MELA_DIR}/data/${SCRAM_ARCH}:${MELA_ANAL_DIR}/CandidateLOCaster/lib/:${MELA_ANAL_DIR}/GenericMEComputer/lib:${MELA_ANAL_DIR}/EventContainer/lib:${MELA_DIR}/data/slc6_amd64_gcc700 
export ROOT_INCLUDE_PATH=$ROOT_INCLUDE_PATH:${MELA_DIR}/interface/
export PYTHONPATH=$PYTHONPATH:${MELA_DIR}/python
export PATH=$PATH:${PWD}/bin
