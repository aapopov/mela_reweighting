#include <iostream>

#include <boost/program_options.hpp>

#include <yaml-cpp/yaml.h>

#include "TFile.h"
#include "TTree.h"
#include <TTreeReader.h>
#include <TTreeReaderArray.h>

#include "Mela.h"

#include <MelaHelper.h>
#include <LHEHandler.h>


namespace po = boost::program_options;


int main(int argc, char *argv[]) {
  bool isVerbose;
  std::string configPath, output, sampleHMass;
  std::vector<std::string> inputs;
  MELAEvent::CandidateVVMode VVMode = MELAEvent::ZZMode;
  int maxEvents, VVDecayMode = 3;

  // Managing program options.
  po::options_description optionsDescription{"Options"};
  optionsDescription.add_options()
    ("help,h", "Help screen")
    ("output,o", po::value<std::string>(&output)->required(), "Output file")
    ("max-events", po::value<int>(&maxEvents)->default_value(-1),
     "Maximum number of events to process.")
    ("config,c", po::value<std::string>(&configPath)->required(),
     "Path to the ME configuration file")
    ("sample-hmass,m", po::value<std::string>(&sampleHMass)->required(),
     "Higgs pole mass of the POWHEG sample")
    ("verbose,v", "Show progress");

  po::options_description hiddenOptionsDescription;
  hiddenOptionsDescription.add_options()
    ("input-files", po::value<std::vector<std::string>>(), "");
  po::positional_options_description posOptionsDescription;
  posOptionsDescription.add("input-files", -1);

  po::options_description allOptionsDescription;
  allOptionsDescription.add(optionsDescription).add(hiddenOptionsDescription);

  po::variables_map options;
  po::store(
      po::command_line_parser(argc, argv).options(allOptionsDescription)
      .positional(posOptionsDescription).run(),
      options);

  if (options.count("help")) {
    std::cerr << "Usage:" << std::endl
      << "mela INPUT_FILE1 [INPUT_FILE2 [...]] [OPTIONS]" << std::endl;
    std::cerr << optionsDescription << std::endl;
    return EXIT_SUCCESS;
  }

  isVerbose = options.count("verbose");
  po::notify(options);

  // If no input file is provided throw an exception.
  if (options["input-files"].empty()) {
    std::ostringstream message;
    message << "No input file is provided. Please pass them as POSITIONAL "
      << "arguments.";
    throw std::runtime_error(message.str());
  }

  inputs = options["input-files"].as<std::vector<std::string>>();
  if( maxEvents == -1)
    maxEvents = INT_MAX;

  // Initialize the Mela object (only one time).
  MelaHelper::SetupMela(125.0, 13.0, TVar::ERROR);

  TFile outputFile(output.c_str(), "RECREATE");

  // Adding input files to the chain.
  TChain chain("Events");
  for (auto &file : inputs)
    chain.Add(file.c_str());

  if (isVerbose)
    std::cout << "Input root files are added into the chain." << std::endl;

  int const nEvents = chain.GetEntries();

  //TTree &eventsTree = *chain.CloneTree(0);
  TTree tree("tree", "tree");
  TTree &eventsTree = tree;

  eventsTree.SetDirectory(&outputFile);

  TTreeReader reader(&chain);
  MelaHelper::MelaHandler melaHandler(eventsTree, sampleHMass, configPath);
  melaHandler.PrepareComputation();

  LHEHandler lheHandler(reader, VVMode, VVDecayMode);

  eventsTree.Branch("2l2nu_mass", &lheHandler.hMass);

  int event_i = 0;
  if (isVerbose)
    std::cout << ((nEvents < maxEvents) ? nEvents : maxEvents)
      << " event(s) will be processed." << std::endl;

  MELACandidate *melaCandidate;
  while(reader.Next()) {
    event_i++;
    if (event_i > maxEvents)
      break;

    if (isVerbose && not (event_i % 5000))
      std::cout << event_i << " events are processed. " << std::endl;

    melaCandidate = lheHandler.GetBestCandidate();
    if (not melaCandidate)
      continue;

    MelaHelper::melaHandle->setCurrentCandidate(melaCandidate);
    
    melaHandler.ComputeClusters();
    melaHandler.PushBranches();

    //int const centry = reader.GetCurrentEntry();
    //chain.GetEntry(centry);
    eventsTree.Fill();
  }

  if (isVerbose)
    std::cout << "Events are processed successfully." << std::endl;

  MelaHelper::ClearMela();
  outputFile.Write();
  outputFile.Close();

  return EXIT_SUCCESS;
}
