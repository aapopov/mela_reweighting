COMPILEPATH = $(PWD)
MAIN_INC_DIR = $(COMPILEPATH)/include
SRC_DIR = $(COMPILEPATH)/src
BIN_DIR = $(COMPILEPATH)/bin
SOURCES = $(SRC_DIR)/*.cc
MELA_LIB_DIR = $(MELA_DIR)/data/${SCRAM_ARCH}
MELA_INC_DIR = $(MELA_DIR)/interface
RUN_DIR = $(COMPILEPATH)
EXE_FILE = mela

CXX = g++
CXXFLAGS = -std=c++17 -g -O2 -Wall -Wextra
CXXFLAGS += $(shell root-config --cflags --glibs)
CXXFLAGS += -I$(MAIN_INC_DIR) -L$(MAIN_LIB_DIR) -lyaml-cpp -lboost_filesystem -lboost_program_options
CXXFLAGS += -I$(MELA_INC_DIR) -L$(MELA_LIB_DIR) -lJHUGenMELAMELA -lcollier -lmcfm_707
CXXFLAGS += -I$(MELA_ANAL_DIR)/CandidateLOCaster/interface -L$(MELA_ANAL_DIR)/CandidateLOCaster/lib/ -lMelaAnalyticsCandidateLOCaster
CXXFLAGS += -I$(MELA_ANAL_DIR)/EventContainer/interface -L$(MELA_ANAL_DIR)/EventContainer/lib/ -lMelaAnalyticsEventContainer
CXXFLAGS += -I$(MELA_ANAL_DIR)/GenericMEComputer/interface -L$(MELA_ANAL_DIR)/GenericMEComputer/lib/ -lMelaAnalyticsGenericMEComputer

.PHONY: all clean

all: $(EXE_FILE)

$(EXE_FILE): $(SRC_DIR)/$(EXE_FILE).cc
	mkdir -p bin
	$(CXX) $(SOURCES) $(CXXFLAGS) -o $(BIN_DIR)/$(EXE_FILE)

clean:
	rm -f $(EXE_FILE)
