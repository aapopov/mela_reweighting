#ifndef LHEHANDLER_H
#define LHEHANDLER_H

#include <TTreeReader.h>
#include <TTreeReaderArray.h>

#include "MELACandidate.h"
#include "MELAEvent.h"

class LHEHandler {
 public:
   // Constructor
   LHEHandler(TTreeReader &reader, MELAEvent::CandidateVVMode VVMode,
       int VVDecayMode);

   // Get the best MELA candidate.
   MELACandidate* GetBestCandidate();

   float hMass;

 private:
   MELAEvent::CandidateVVMode VVMode_;

   int VVDecayMode_;

   std::unique_ptr<MELACandidate> bestCandidate_;

   std::unique_ptr<MELAEvent> melaEvent_;

   std::vector<std::unique_ptr<MELAParticle>> stableParticles_, mothers_;

   TTreeReaderArray<float> ptArray_, etaArray_, phiArray_, massArray_;

   TTreeReaderArray<float> incomingPzArray_;

   TTreeReaderArray<int> pdgIdArray_, statusArray_;

   TTreeReaderValue<unsigned int> numPart_;

   // Build the best candidate based on VV mode and VV decay mode
   void BuildBestCandidate();
};

#endif
